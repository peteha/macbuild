#!/bin/sh
brew install python3
brew install pip3
brew install jq
brew install broot

brew cask install dropbox
brew cask install 1password
brew bask install lastpass
brew cask install pycharm
brew cask install visual-studio-code
brew cask install spotify
brew cask install omnigraffle
brew cask install tower
brew cask install clover-configurator
brew cask install wireshark
brew cask install whatroute
brew cask install firefox
brew cask install istat-menus
brew cask install textmate
brew cask install zoc
brew cask install google-chrome
brew cask install forklift
brew cask install keyboard-maestro
brew cask install malwarebytes
brew cask install kite


sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
